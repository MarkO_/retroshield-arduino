# RetroShield 8085 for Arduino Mega

These are the software project folders for the RetroShield 8085.

* `k8085_test`: bring-up & validation of RetroShield 8031.
* `k8085_mon85`: MON85 monitor code by Dave Dunfield, modified by Roman Borik.